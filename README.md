#Use this repo to set up EC2 Instance using:
	UserData
	CloudFormation Helper Scripts
	CloudFormationInit

## Terminology used in CloudFormation

	 1. Templates
	 2. Stacks
	 3. Change Sets

## To Run the template:
	
	1. Create a stack in one of the regions: 
       us-east-1:
       us-west-1:
       eu-west-1:
       ap-southeast-1:
       ap-southeast-2:
  	
  	2. Upload template in to the stack

  	3. This will create t2.micro ec2 instance, however the size of the instance can be canged
  	   by changing InstanceType: t2.micro to for example t2.small

  	4. By Changing the values of:
  	   MinSize: 1
       MaxSize: 3
       You can set up as many instances as you need


